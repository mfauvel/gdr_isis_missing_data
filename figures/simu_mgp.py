# From https://scikit-learn.org/stable/auto_examples/gaussian_process/plot_gpr_prior_posterior.html
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ExpSineSquared

np.random.seed(10)
# Define GPRs
kernels = [2.0 * ExpSineSquared(
    length_scale=l,
    periodicity=p,)
           for l, p in zip([2.0, 6.0],[2.0, 6.0])]

gprs = [GaussianProcessRegressor(kernel=kernel, random_state=k)
        for k, kernel in enumerate(kernels)]

fig, axs = plt.subplots(nrows=2, sharex=True, sharey=True, figsize=(10, 8))
A = 0.5 - np.random.rand(2, 2)
A /= A.sum(axis=1, keepdims=True)
print(A)

# plot independant GPs
t = np.linspace(0, 5, 100)
t = t.reshape(-1, 1)
W = []
for gpr in gprs:
    # plot_gpr_samples(gpr, n_samples=1, ax=axs[0])
    w = gpr.sample_y(t, 1)
    axs[0].plot(
        t,
        w,
        linestyle="--",
        alpha=0.7,
    )
    W.append(w)
axs[0].set_xlabel("t")
axs[0].set_ylabel("W(t)")
axs[0].set_ylim([-3, 3])

# PLot LMC
M = np.array([
    t*0.25,
    -t*0.15+1
]).squeeze()

W = np.array(W).squeeze()
Y = np.dot(A, W) + M
for idx in range(2):
    axs[1].plot(
        t,
        Y[idx, :],
        linestyle="--",
        alpha=0.7,
    )
pd.DataFrame.from_dict({"time":t.squeeze(),
                        "w0":W[0,:],
                        "w1":W[1,:],
                        "y0":Y[0,:],
                        "y1":Y[1,:]                       
                        }).to_csv("simu_mgp.csv", index=False)
plt.show()


